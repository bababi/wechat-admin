const errCode = {
  noErr: 0,
  errOnGetInstaclipData: 1301
};

let httpPort;
let httpsPort;
const mysqlConfig = {
  host: '127.0.0.1',
  user: 'root',
  password: 'root',
  database: 'wechat',
  charset: 'UTF8MB4_GENERAL_CI'
};

const yunConfig = {
  accessKeyId: 'LTAIz3eYg0ezdwty',
  secretAccessKey: 'BXLCPApmwLEwg48ffIzIOnX4lGV1eK',
  host: 'https://liuliu-game-box.oss-cn-shenzhen.aliyuncs.com'
};

if (process.env.NODE_ENV === 'development') {
  httpPort = 3000;
  httpsPort = 3001;
  yunConfig.startsWith = 'test/';
} else if (process.env.NODE_ENV === 'production') {
  httpPort = 80;
  httpsPort = 6666;
  mysqlConfig.host = '172.18.126.186';
  yunConfig.startsWith = 'img/';
} else if (process.env.NODE_ENV === 'stg') {
  mysqlConfig.host = '120.77.200.245';
  httpPort = 3000;
  httpsPort = 3001;
  yunConfig.startsWith = 'test/';
} else {
  httpPort = 80;
  httpsPort = 443;
  yunConfig.startsWith = 'img/';
}

const envConfig = {
  httpPort,
  httpsPort,
  nodeEnv: process.env.NODE_ENV
};

module.exports = {
  envConfig,
  errCode,
  mysqlConfig,
  yunConfig
};
