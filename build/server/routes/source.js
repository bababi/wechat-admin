const express = require('express');
const router = express.Router();
const supportSource = {box_games: 1, img_tag: 1, img_repo: 1, app_configs: 1};
const parseRes = require('../tools/parseRes');
const {deleteById, updateById, add, list, queryById} = require('../mysql/util');

router.post('/add/:table', async function(req, res, next) {
  const table = req.params.table;
  // 未在支持序列
  if (!supportSource[table]) {
    res.send(parseRes.parseError(parseRes.ERROR_TABLE_NOT_FOUND, parseRes.ERROR_TABLE_NOT_FOUND_MSG));
    return;
  }
  let data = req.body.data;
  if (typeof data === 'string') {
    try {
      data = JSON.parse(data);
    } catch (e) {
      console.log('LOGGER EOORR:', e);
    }
  }
  const result = await add({table, data});
  if (result.data === -1) {
    res.send(parseRes.parseError(parseRes.ERROR_SQL, parseRes.ERROR_SQL_MSG));
  } else {
    res.send(parseRes.parseSuccess(result));
  }
});

router.post('/deleteById/:table/:id', async (req, res, next) => {
  const table = req.params.table;
  const id = req.params.id;
  // 未在支持序列
  if (!supportSource[table]) {
    res.send(parseRes.parseError(parseRes.ERROR_TABLE_NOT_FOUND, parseRes.ERROR_TABLE_NOT_FOUND_MSG));
    return;
  }
  const result = await deleteById({table, id});
  if (result === -1) {
    res.send(parseRes.parseError(parseRes.ERROR_SQL, parseRes.ERROR_SQL_MSG));
  } else {
    res.send(parseRes.parseSuccess(result));
  }
});

router.post('/queryById/:table/:id', async (req, res, next) => {
  const table = req.params.table;
  const id = req.params.id;
  // 未在支持序列
  if (!supportSource[table]) {
    res.send(parseRes.parseError(parseRes.ERROR_TABLE_NOT_FOUND, parseRes.ERROR_TABLE_NOT_FOUND_MSG));
    return;
  }
  const result = await queryById({table, id});
  if (result === -1) {
    res.send(parseRes.parseError(parseRes.ERROR_SQL, parseRes.ERROR_SQL_MSG));
  } else {
    res.send(parseRes.parseSuccess(result));
  }
});

router.post('/updateById/:table/:id', async (req, res, next) => {
  const table = req.params.table;
  const id = req.params.id;
  // 未在支持序列
  if (!supportSource[table]) {
    res.send(parseRes.parseError(parseRes.ERROR_TABLE_NOT_FOUND, parseRes.ERROR_TABLE_NOT_FOUND_MSG));
    return;
  }
  let data = req.body.data;
  if (typeof data === 'string') {
    try {
      data = JSON.parse(data);
    } catch (e) {
      console.log('LOGGER EOORR:', e);
    }
  }
  const result = await updateById({table, data, id});
  if (result === -1) {
    res.send(parseRes.parseError(parseRes.ERROR_SQL, parseRes.ERROR_SQL_MSG));
  } else {
    res.send(parseRes.parseSuccess(result));
  }
});

router.post('/list/:table', async function(req, res, next) {
  const table = req.params.table;
  // 未在支持序列
  if (!supportSource[table]) {
    res.send(parseRes.parseError(parseRes.ERROR_TABLE_NOT_FOUND, parseRes.ERROR_TABLE_NOT_FOUND_MSG));
    return;
  }
  const {columns, filters, orderBy, pageNum, pageSize} = req.body;
  const result = await list({
    table,
    columns,
    filters,
    pageNum,
    pageSize,
    orderBy
  });
  if (result === -1) {
    res.send(parseRes.parseError(parseRes.ERROR_SQL, parseRes.ERROR_SQL_MSG));
  } else {
    res.send(parseRes.parseSuccess(result));
  }
});

module.exports = router;
