const renderToString = require('../tools/react-render').renderToString;
const Admin = require('../view/Admin');
const Combine = require('../view/Combine');
module.exports = function setup(app) {
  app.get('/', (req, res) => {
    res.send(renderToString(Admin, {}));
  });

  app.get('/combine', (req, res) => {
    res.send(renderToString(Combine, {}));
  });
};
