const express = require('express');
const router = express.Router();
const {genSignature} = require('../tools/ossSignature.js');

router.get('/ossSignature', (req, res, next) => {
  const obj = genSignature();
  const parseRes = require('../tools/parseRes');
  res.send(parseRes.parseSuccess(obj));
});

module.exports = router;
