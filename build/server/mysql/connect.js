const mysql = require('mysql');
const config = require('../../../config/application.config').mysqlConfig;
let conn;

function connect() {
  conn = mysql.createPool(config);
}

module.exports = {
  connect,
  getConn: () => {
    return conn;
  }
};
