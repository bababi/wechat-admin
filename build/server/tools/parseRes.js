module.exports = {
  /*******用户模块00xx******************/
  // 未登录
  ERROR_SESSIONID: '0001',
  ERROR_SESSIONID_MSG: '未登录',
  // 读取session失败
  ERROR_READ_SESSION: '0002',
  ERROR_READ_SESSION_MSG: '读取session失败',
  // sql 错误
  ERROR_SQL: '0003',
  ERROR_SQL_MSG: 'sql 异常',
  // table not found
  ERROR_TABLE_NOT_FOUND: '0004',
  ERROR_TABLE_NOT_FOUND_MSG: '错误的表名',
  // wechat auth fail
  ERROR_WX_AUTH_ERROR: '0005',
  ERROR_WX_AUTH_ERROR_MSG: '微信授权失败',
  // 非法手机号
  ERROR_MOBILE_ERROR: '0006',
  ERROR_MOBILE_ERROR_MSG: '非法手机号',
  // 验证码错误
  ERROR_CAPTCHA_CODE: '0007',
  ERROR_CAPTCHA_CODE_MSG: '验证码错误',
  // sms error
  ERROR_SMS_ERROR: '0008',
  ERROR_SMS_ERROR_MSG: '验证码发送失败，请稍后重试',
  // 封装返回结果
  parseSuccess: function(data) {
    var result = {
      code: '0',
      msg: '',
      data: data
    };
    return JSON.stringify(result);
  },
  parseError: function(code, msg) {
    var result = {
      code: code,
      msg: msg
    };
    return JSON.stringify(result);
  }
};
