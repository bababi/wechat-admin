# wechat-server
## 一、usage
1. 安装 node 环境，要求 v8.0。建议安装 nvm，可以方便切换node版本
2. npm install
3. npm run stg (连接的测试环境数据库)
4. http://localhost:3000/

## 二、如果需要搭建本地的数据库运行
1. 安装 [mysql](https://dev.mysql.com/downloads/mysql/)，v5.7 版本，并启动
2. 执行 sql/wechat.sql 创建数据库实例 wechat
3. 执行 `mysql -uroot -p wechat < [your project patch]/sql/dumpout.sql`, 同步线上数据结构以及数据
4. npm run dev
5. http://localhost:3000/

## 三、项目结构介绍
### 简介
本项目基于 node(express)、react(react、react-router、redux、antd 等)、mysql 搭建，基于 react 的服务器端渲染，可以支持一定程度上的前后端同构

### 前后端交互用例图
![用例图](https://upload-images.jianshu.io/upload_images/2954145-8724b4072528575f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 项目目录说明
```
|-- build                        --------编译目录，npm run build 的产物
|-- .ssl                         --------ssl 证书，用于 https，不需要 care
|-- config
  |-- application.config.js      --------项目全局配置，例如：mysql 连接参数
  |-- webpack.config.common.js   --------webpack 公共配置
  |-- webpack.config.dev.js      --------webpack 开发环境配置（npm run dev / npm run stg）
  |-- webpack.config.prod.js     --------webpack 生产环境配置 (npm run build)
  |-- webpck.config.server.js    --------webpack 服务端编译配置（npm run reactServer）
|-- test                         --------测试用例目录
|-- sql                          --------数据库创建脚本，数据备份脚本（跑 npm run stg 不需要 care）
|-- .babelrc                     --------babel 配置
|-- .eslintrc                    --------eslint 配置，静态代码检查（注意代码规范，哥哥姐姐们）
|-- .gitignore
|-- README.md                    --------本文档
|-- src                          
  |--client
    |--*.js                      --------react 客户端主文件 （webpack 编译入口文件）
    |--assets                    --------静态资源文件夹，现在主要用来存放图片
    |--pages                     --------页面的入口，编译到 server/views 下，服务器端渲染之后的 html 载体
    |--containers                --------react 容器组件，之所以叫做容器，是因为会注入 redux 状态（数据）
    |--store                     --------redux 相关
    |--components                --------组件目录
    |--utils                     --------工具方法
  |--server
    |--middlewares               --------express 中间件/路由
    |--mysql                     --------数据库连接，以及相关操作
    |--routes                    --------express路由
    |--tools                     --------工具方法
    |--view                      --------npm run reactServer 编译产物，react 服务器端渲染
    |--index.js                  --------应用入口文件，启动 express 服务等等
    |--logger.js                 --------日志操作

```

### 编译过程介绍
以 `npm run stg` 为例，实际执行的命令为 `npm run reactServer && cross-env NODE_ENV=stg node src/server`，这其实分为两个个步骤：

1）编译服务端渲染的 react 文件
```
npm run reactServer -> cross-env NODE_ENV=development webpack -p --progress --config=config/webpack.config.server.js
```
基于 webpakc.config.server 进行编译，target 为 node，因为要在服务器端 node 环境中运行

2） 启动 node express 服务器（包括建立跟 mysql 的连接），并基于 webpackDevMiddleware 和 webpackHotMiddleware 编译 target 为 browser 的 react 文件，用户浏览器运行使用，同时启动对前端文件修改的热更新监听。

`npm run build` 跟 `npm run stg/npm run dev` 略有不同，会将服务器端 react 文件编译，并把服务器相关文件 copy 到 build 目录，然后再编译浏览器端 react 文件

**注意：虽然前后端 react 各自编译（因为运行的 target 不同），但是共用大部分源文件，所以是某种程度上的同构**

## 二、通用资源api
### 通用列表查询接口
1. url: /api/source/list/:table，table 为要查询的表名
2. method: POST
3. body 参数
```
  // columns: 查询字段集合，PS: openid/unionid 不允许查询，填写也无用
  ['id', 'nickName']

  // filters: where 条件
  [
    {
      column: '', // 列名
      filterType: 'equal', // 过滤类型
      value: '' // 值
    }
  ]
  // 支持的过滤类型有：
  <!-- const filterKeys = {
    equal: '=',
    noEqual: '<>',
    greater: '>',
    less: '<',
    greaterEqual: '>=',
    lessEqual: '<=',
    between: 'between',
    like: 'like'
  }; -->

  // orderBy 排序方式，默认 inTime desc
  {
    column: '', // 列名
    type: 'desc' // 排序方式
  }

  // pageNum 第几页, 默认为 1
  pageNum = 1

  // pageSize 每页查多少条，默认为 10
  pageSize = 10
```
4. 查询举例：
![](https://upload-images.jianshu.io/upload_images/2954145-57aa996fd9c754af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 通用插入数据接口
1. url: /api/source/add/:table
2. method: POST
3. body 参数
```
// json
{
  data: {}, // key 为列名，value 为列的值（id、inTime、updateTime 服务器端缺省生成，无需传入）
}
// 特别的，unionid、openid 只需要写死 unionid:1、openid:1，后台即可自动注入

```
4. 调用举例
requestUrl: http://127.0.0.1:3000/wechat/source/add/score
body(json):
```
{
    "data":{
        "unionid": 1, // 后台自动判断并注入
        "app":"2048",
        "score":100
    }
}
```
### 通用更新数据接口
1. url: /api/source/updateById/:table/:id
2. method: POST
3. body 参数
```
// json
{
  data: {}, // key 为列名，value 为列的值
}
```
4. 调用举例：
requestUrl: http://127.0.0.1:3000/wechat/source/updateById/score/d2ba12c0-a1e6-11e8-bf60-bfa69f8a584e
body(json):
```
{
    "data":{
        "score":1024
    }
}
```
### 通用数据删除接口
1. url: /api/source/deleteById/:table/:id
2. method: POST
3. 调用举例：http://127.0.0.1:3000/wechat/source/deleteById/score/d2ba12c0-a1e6-11e8-bf60-bfa69f8a584e

## 三、接口说明
### 获取用户信息
1. url: /user/getUserInfo
2. method: get
3. 返回值
```
  // 未登录
  {
    code: '001',
    msg: '未登录'
  }

  // 登录后
  {
    code: '0',
    msg: '',
    data: {
      mobile: 13128872736
    }
  }
```
4. 访问举例：http://localhost:3000/user/getUserInfo
### 获取验证码
1. url: /user/getCode
2. method: get
3. params: mobile
4. 返回值
```
// 手机号在白名单内
{
  code: '0',
  msg: '',
  data: {}
}

// 手机不在白名单内
{
  code: '006',
  msg: '非法手机号'
}
```
5. 访问举例：http://localhost:3000/user/getCode?mobile=13128872733
### 登录接口
1. url: /user/login
2. method: post
3. params
```
{
  mobile: '13128872736',
  captchaCode: '7788'
}
```
4. 返回值
```
// 校验不通过
{
  code: '0007',
  msg: '验证码错误'
}

// 校验通过
{
  code: '0',
  msg: '',
  data: {
    mobile: 13128872736
  }
}
```