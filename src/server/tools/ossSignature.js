const {accessKeyId, host, secretAccessKey, startsWith} = require('../../../config/application.config.js').yunConfig;
const crypto = require('crypto');

function genSignature() {
  let end = new Date().getTime() + 1000 * 60 * 3;
  let expiration = new Date(end).toISOString();
  let policyString = {
    expiration,
    conditions: [['content-length-range', 0, 1048576000], ['starts-with', '$key', startsWith]]
  };
  policyString = JSON.stringify(policyString);
  const policy = new Buffer(policyString).toString('base64');
  const signature = crypto
    .createHmac('sha1', secretAccessKey)
    .update(policy)
    .digest('base64');
  return {
    accessKeyId,
    host,
    policy,
    signature,
    saveName: end,
    startsWith
  };
}

module.exports = {
  genSignature
};
