const SMSClient = require('./sms-client.js');
const {accessKeyId, secretAccessKey} = require('../../../config/application.config.js').yunConfig;
const smsClient = new SMSClient({accessKeyId, secretAccessKey});

module.exports = async function(phone, code) {
  let res;
  try {
    res = await smsClient.sendSMS({
      PhoneNumbers: phone,
      SignName: '古典2048',
      TemplateCode: 'SMS_146800852',
      TemplateParam: `{"code":"${code}"}`
    });
  } catch (e) {
    console.log(e);
    return -1;
  }
  return res;
};
