const parseRes = require('../tools/parseRes');
module.exports = async function(req, res, next) {
  const userInfo = req.session.userInfo;
  if (!userInfo || !userInfo.auth) {
    res.send(parseRes.parseError(parseRes.ERROR_SESSIONID, parseRes.ERROR_SESSIONID_MSG));
  } else {
    next();
  }
};
