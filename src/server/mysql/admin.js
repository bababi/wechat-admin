const {query} = require('./util');

async function checkMobile(mobile) {
  const res = await query({
    sql: 'select id from admin where mobile=?',
    params: [mobile]
  });
  console.log(mobile, res);
  if (res && res.length > 0) {
    return true;
  }
  return false;
}

module.exports = {
  checkMobile
};
