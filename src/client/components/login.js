import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Modal, Form, Icon, Input, Button, message} from 'antd';
import axios from 'axios';
import styles from './login.scss';
const FormItem = Form.Item;
@Form.create()
class Login extends React.Component {
  state = {
    visible: true,
    isDisabled: false,
    count: 60
  };
  componentDidMount() {
    // console.log(this.props)
  }
  // 获取验证码
  getCode = async (e) => {
    const mobile = this.props.form.getFieldValue('phoneNumber');
    if (!mobile) {
      message.info('请输入手机号！');
    } else {
      const res = await axios.get(`/user/getCode?mobile=${mobile}`);
      if (res.status == 200) {
        if (res.data.code != '0') {
          message.error('请确认你的手机号合法！');
        }
        this.timer = setInterval(
          function() {
            var count = this.state.count;
            this.state.isDisabled = true;
            count -= 1;
            if (count < 1) {
              this.setState({
                isDisabled: false
              });
            }
            this.setState({
              count: count
            });
          }.bind(this),
          1000
        );
      }
    }
  };
  handleOk = async (e) => {
    const msg = this.props.form.getFieldsValue();
    let res = await axios.post('/user/login', {mobile: msg.phoneNumber, captchaCode: msg.captchaCode});
    if (res.status == '200') {
      if (res.data.code == '0007') {
        message.info('验证码不正确！');
      } else if (res.data.code == '0') {
        this.props.transmit(true);
      }
    }
  };
  handleCancel = (e) => {
    this.setState({
      visible: true
    });
  };
  maskClick = () => {
    this.setState({
      visible: true
    });
  };
  render() {
    const {getFieldDecorator} = this.props.form;
    var text = this.state.isDisabled ? this.state.count + '秒后重发' : '获取验证码';
    return (
      <div onClick={this.maskClick}>
        <Modal title="请您先登录" visible={this.state.visible} onOk={this.handleOk} onCancel={this.handleCancel}>
          <Form className="login-form">
            <FormItem>
              {getFieldDecorator('phoneNumber', {
                rules: [{required: true, message: 'Please input your phoneNumber!'}]
              })(<Input prefix={<Icon type="phone" theme="outlined" />} placeholder="请输入手机号码" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('captchaCode', {
                rules: [{required: true, message: 'Please input your Password!' }]
              })(
                <Input
                  prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}} />}
                  style={{width: '60%'}}
                  type="password"
                  placeholder="验证码"
                />
              )}
              <Button
                style={{marginLeft: '85px'}}
                type="primary"
                disabled={this.state.isDisabled}
                onClick={() => this.getCode()}
              >
                {text}
              </Button>
            </FormItem>
          </Form>
        </Modal>
      </div>
    );
  }
}
function select(state) {
  return state;
}
export default withRouter(connect(select)(Login));