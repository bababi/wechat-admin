import React from 'react';
import styles from './style.scss';
import {Layout, Menu} from 'antd';
const {Header} = Layout;

export default class MainHeader extends React.Component {
  render() {
    return (
      <Header className="header">
        <div className={styles.logo}>溜溜游戏</div>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']} style={{lineHeight: '64px'}}>
          <Menu.Item key="1">通用</Menu.Item>
        </Menu>
      </Header>
    );
  }
}
