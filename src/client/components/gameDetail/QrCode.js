import React from 'react';
import styles from './qrCode.scss';

export default class QrCode extends React.Component {
  render() {
    const {qrCode} = this.props;
    return (
      <div className={styles.wrap}>
        <div className={styles.qrCodeWrap}>
          <img src={qrCode} className={styles.qrCode} />
        </div>
        <div className={styles.line} />
        <div className={styles.title}>长按识别开始游戏</div>
        <div className={styles.tip}>搜索小程序“溜溜玩盒子”体验更多精美小游戏</div>
        <div className={styles.pl} />
      </div>
    );
  }
}
