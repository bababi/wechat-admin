import React from 'react';
import styles from './topArea.scss';
class TopArea extends React.Component {
  render() {
    const {name, icon, tags} = this.props;
    return (
      <div className={styles.wrap}>
        <img src={icon} className={styles.icon} />
        <div className={styles.nameWrap}>
          <div className={styles.name}>{name}</div>
          <div className={styles.tags}>{tags}</div>
        </div>
      </div>
    );
  }
}

export default TopArea;
