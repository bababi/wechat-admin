import React from 'react';
import {Menu, Icon} from 'antd';
const {SubMenu} = Menu;
import {Link} from 'react-router-dom';

class Nav extends React.Component {
  go = (path) => {
    this.props.history.replace(path);
  };

  render() {
    return (
      <Menu
        mode="inline"
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1']}
        style={{height: '100%', borderRight: 0}}
      >
        <SubMenu
          key="sub1"
          title={
            <span>
              <Icon type="folder" theme="outlined" />游戏盒子
            </span>
          }
        >
          <Menu.Item key="1">
            <Link to="/gameBox">游戏列表</Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to="/appConfigs">应用配置</Link>
          </Menu.Item>
        </SubMenu>
      </Menu>
    );
  }
}

export default Nav;
