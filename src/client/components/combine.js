import React from 'react';
import styles from './combine.scss';
import MainHeader from './Header';
import {Button} from 'antd';
import html2canvas from 'html2canvas';
import TopArea from './gameDetail/TopArea';
import QrCode from './gameDetail/QrCode';
import {getQuery} from 'utils/common';
export default class Combine extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataUrl: ''
    };
  }

  async componentDidMount() {}

  async dueCanvas() {
    const width = 375;
    const height = 667;
    const canvas = document.createElement('canvas');
    canvas.width = width * 2;
    canvas.height = height * 2;
    canvas.style.width = width + 'px';
    canvas.style.height = height + 'px';
    const context = canvas.getContext('2d');
    context.scale(2, 2);
    const c = await html2canvas(document.querySelector('#preview'), {
      canvas
    });
    const base64 = c.toDataURL('image/jpeg', 1);
    this.setState({
      dataUrl: base64
    });
  }

  async download() {
    await this.dueCanvas();
    this.refs.link.click();
  }

  render() {
    const query = getQuery();
    const {name, tags, icon, qrCode} = query;
    return (
      <div className={styles.container}>
        <MainHeader />
        <div className={styles.mainContent}>
          <div className={styles.preview}>
            <div className={styles.previewInner} id="preview">
              <TopArea name={name} tags={tags} icon={icon} />
              <QrCode qrCode={qrCode} />
            </div>
            <div className={styles.handlers}>
              <Button
                type="primary"
                shape="circle"
                icon="download"
                size={'large'}
                onClick={() => {
                  this.download();
                }}
              />
              <a ref="link" href={this.state.dataUrl} download="qrCode" className={styles.link}>
                下载
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
