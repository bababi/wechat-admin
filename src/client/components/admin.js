import React from 'react';
import {connect} from 'react-redux';
import {Route, withRouter} from 'react-router-dom';
import {Layout, Breadcrumb} from 'antd';
const {Content, Sider} = Layout;
import Nav from './Nav';
import '../containers/common.scss';
// import styles from './admin.scss';
import GameBox from './GameBox';
import Editor from './GameBox/editor';
import AppConfigs from './AppConfigs';
import MainHeader from './Header';
import AppConfigsEditor from './AppConfigs/editor';
class Admin extends React.Component {
  buildBread() {
    const pathName = this.props.location.pathname;
    if (pathName === '/') return <Breadcrumb.Item>Home</Breadcrumb.Item>;
    const paths = pathName.split('/');
    const result = [];
    for (let i = 0; i < paths.length; i++) {
      const path = paths[i] === '' ? 'Home' : paths[i];
      result.push(<Breadcrumb.Item key={path}>{path}</Breadcrumb.Item>);
    }
    return result;
  }

  render() {
    return (
      <Layout>
        <MainHeader />
        <Layout>
          <Sider width={200} style={{background: '#fff'}}>
            <Nav history={this.props.history} />
          </Sider>
          <Layout style={{padding: '0 24px 24px'}}>
            <Breadcrumb style={{margin: '16px 0'}}>{this.buildBread()}</Breadcrumb>
            <Content style={{background: '#fff', padding: 24, margin: 0}}>
              <Route path="/" component={GameBox} exact />
              <Route path="/gameBox" component={GameBox} exact />
              <Route path="/gameBox/editor" component={Editor} exact />
              <Route path="/appConfigs" component={AppConfigs} exact />
              <Route path="/appConfigs/editor" component={AppConfigsEditor} exact />
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

function select(state) {
  return state;
}

export default withRouter(connect(select)(Admin));
