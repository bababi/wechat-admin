import React from 'react';
import {Modal} from 'antd';
import {connect} from 'react-redux';
import actions from 'store/actions/img-repo';
import styles from './style.scss';
import ImgList from './img-list';
import Upload from './upload';
import TagList from './tag-list';
class ImgRepo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: false
    };
  }

  onSelect(url) {
    this.setState({
      url
    });
  }

  handleOk() {
    this.handleCancel();
    if (this.state.url) {
      this.props.onChange && this.props.onChange(this.state.url);
    }
  }

  handleCancel() {
    const {onClose} = this.props;
    onClose && onClose();
  }

  componentWillMount() {
    const {dispatch} = this.props;
    dispatch(actions.queryImgTags());
    dispatch(actions.queryImgsByTag('all'));
  }

  render() {
    const visible = this.props.visible;
    const {imgs, tags, currTag, imgTotal, currPage} = this.props.imgRepoState;
    return (
      <Modal
        title="选择图片"
        visible={visible}
        onOk={() => {
          this.handleOk();
        }}
        onCancel={() => {
          this.handleCancel();
        }}
        width={800}
        height={500}
      >
        <div className={styles.container}>
          <div className={styles.tags}>
            <TagList dispatch={this.props.dispatch} tags={tags} />
          </div>
          <div className={styles.main}>
            <Upload btnId={this.props.btnId} type="primary" dispatch={this.props.dispatch} currTag={currTag}>
              上传图片
            </Upload>
            <ImgList
              currPage={currPage}
              imgs={imgs}
              total={imgTotal}
              dispatch={this.props.dispatch}
              currTag={currTag}
              onSelect={(url) => {
                this.onSelect(url);
              }}
            />
          </div>
        </div>
      </Modal>
    );
  }
}

function select(state) {
  return state;
}

export default connect(select)(ImgRepo);
