const types = require('../action-types.js');
const {request} = require('utils/request');

// 获取游戏列表
function getGameList() {
  return async (dispatch) => {
    const res = await request({
      url: 'api/source/list/box_games',
      method: 'post',
      data: {
        columns: ['id', 'name', 'icon', 'slogan', 'status', 'seq'],
        pageSize: 100
      }
    });
    if (res.code === '0') {
      dispatch({
        type: types.GET_BOX_LIST,
        data: res.data
      });
    }
  };
}

// 添加游戏
function addGame(data) {
  return async (dispatch) => {
    const res = await request({
      url: 'api/source/add/box_games',
      method: 'post',
      data: {
        data
      }
    });
    return res;
  };
}

// 修改游戏
function editGame(id, data) {
  return async (dispatch) => {
    const res = await request({
      url: `api/source/updateById/box_games/${id}`,
      data: {
        data
      },
      method: 'post'
    });
    return res;
  };
}

// 根据id查询
function queryById(id) {
  return async (dispatch) => {
    const res = await request({
      url: `api/source/queryById/box_games/${id}`,
      method: 'post'
    });
    return res;
  };
}

// 根据id删除
function deleteById(id) {
  return async (dispatch) => {
    const res = await request({
      url: `api/source/deleteById/box_games/${id}`,
      method: 'post'
    });
    return res;
  };
}

module.exports = {
  getGameList,
  addGame,
  queryById,
  editGame,
  deleteById
};
