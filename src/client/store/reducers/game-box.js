const types = require('../action-types.js');
const defaultState = {
  list: {}
};

module.exports = function gameBoxState(state = defaultState, action) {
  switch (action.type) {
    case types.GET_BOX_LIST:
      return Object.assign({}, state, {list: action.data});
    default:
      return state;
  }
};
