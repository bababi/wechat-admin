const types = require('../action-types.js');

const defaultImgRepoState = {
  tags: [],
  imgs: [],
  imgTotal: 0,
  currPage: 1,
  currTag: 'all'
};

function imgRepoState(state = defaultImgRepoState, action) {
  switch (action.type) {
    case types.SET_IMG_TAGS:
      return Object.assign({}, state, {tags: action.tags});
    case types.SET_REPO_IMGS:
      return Object.assign({}, state, {imgs: action.data.list, imgTotal: action.data.total});
    case types.SET_IMG_PAGE:
      return Object.assign({}, state, {currPage: action.page});
    case types.SET_CURR_TAG:
      return Object.assign({}, state, {currTag: action.tag});
    default:
      return state;
  }
}

module.exports = imgRepoState;
