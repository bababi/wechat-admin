const types = require('../action-types.js');

const defaultAppState = {
  imgRepoVisible: true // 图库是否可见
};

function appState(state = defaultAppState, action) {
  switch (action.type) {
    case types.SWITCH_IMG_REPO:
      return Object.assign({}, state, {imgRepoVisible: action.imgRepoVisible});
    default:
      return state;
  }
}

module.exports = appState;
