const types = require('../action-types.js');
const defaultState = {
  configs: [],
  currentConfig: {}
};

module.exports = function appConfigsState(state = defaultState, action) {
  switch (action.type) {
    case types.SET_APP_CONFIGS:
      return Object.assign({}, state, {configs: action.configs});
    case types.SET_CURRENT_CONFIG:
      return Object.assign({}, state, {currentConfig: action.config});
    default:
      return state;
  }
};
