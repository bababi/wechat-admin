const combineReducers = require('redux').combineReducers;
const appState = require('./app');
const imgRepoState = require('./img-repo');
const gameBoxState = require('./game-box');
const appConfigsState = require('./app-configs');
module.exports = combineReducers({
  appState,
  imgRepoState,
  gameBoxState,
  appConfigsState
});
