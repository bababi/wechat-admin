import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Login from '../components/login';
import Admin from '../components/admin';
import './common.scss';
import axios from 'axios';

class Index extends React.Component {
  state = {
    isLogin: false
  };
  componentDidMount() {
    axios.get('/user/getUserInfo').then((res) => {
      if (res.status === 200) {
        if (res.data.code == '1') {
          this.setState({
            isLogin: false
          });
        } else if (res.data.code == 0) {
          this.setState({
            isLogin: true
          });
        }
      }
    });
  }
  transmit(isLogin) {
    this.setState({
      isLogin: isLogin
    });
  }
  render() {
    const isLogin = this.state.isLogin;
    let content = null;
    if (isLogin) {
      content = <Admin />;
    } else {
      content = <Login transmit={(isLogin) => this.transmit(isLogin)} />;
    }
    return content;
  }
}

function select(state) {
  return state;
}

export default withRouter(connect(select)(Index));
