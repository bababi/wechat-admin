import React from 'react';
import {connect} from 'react-redux';
import {Route, withRouter} from 'react-router-dom';
import './common.scss';

class HomePage extends React.Component {
  render() {
    return <div className="root">hello world!!!</div>;
  }
}

function select(state) {
  return state;
}

export default withRouter(connect(select)(HomePage));
