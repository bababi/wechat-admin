import React from 'react';
import ReactDOM from 'react-dom';
import Combine from 'comps/combine.js';

ReactDOM.render(<Combine />, document.getElementById('app'));
