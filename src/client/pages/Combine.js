import React, {Component} from 'react';
import BasePage from './Layout';

export default class Combine extends Component {
  render() {
    return <BasePage bundleName="combine" />;
  }
}
